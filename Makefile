IMAGES=images/allc_recoPFCandidates_particleFlow__RECO_obj_eta17.png images/allc_EcalRecHitsSorted_reducedEcalRecHitsEE__RECO_obj_obj_chi2.png
IMAGES+=images/wlcg_pilot_overview.png images/wlcg_schema.png
IMAGES+=images/chart_jobs.png images/arm_site_works_3.png

all: clean $(IMAGES)
	pdflatex linaro_15_aarch64_wlcg.tex
	bibtex linaro_15_aarch64_wlcg
	pdflatex linaro_15_aarch64_wlcg.tex
	pdflatex linaro_15_aarch64_wlcg.tex

clean:
	rm -f *.bbl *.aux *.log *.pdf *.out *.blg
