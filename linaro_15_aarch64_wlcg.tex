\documentclass[a4paper]{jpconf}
\usepackage{graphicx}
\usepackage{lineno}
\usepackage{hyperref}
\usepackage{color}
\usepackage{url}

\begin{document}

\title{ARM64/AArch64 for Scientific Computing at the CERM CMS Particle Detector}

\author{David Abdurachmanov$^1$, Brian Bockelman $^2$, Justas Balcas$^3$,
Peter Elmer$^4$, Robert Knight$^5$, }

\address{$^1$ Fermilab, Batavia, IL 60510, USA}
\address{$^2$ University of Nebraska-Lincoln, Lincoln, NE 68588, USA}
\address{$^3$ California Institute of Technology, Pasadena, CA 91125, USA}
\address{$^4$ Department of Physics, Princeton University, Princeton, NJ 08540,
USA}
\address{$^5$ Research Computing, Office of Information Technology, Princeton
University, Princeton, NJ 08540, USA}

\ead{David.Abdurachmanov@cern.ch}

\begin{abstract}
The purpose of this paper is to provide an overview of efforts at CERN (the European 
Laboratory for Particle Physics, in Geneva, Switzerland) to introduce ARMv8 64-bit 
(aka AArch64) for large scale scientific computing. The objective is to crunch data 
from the 14 000 ton CMS particle physics detector located 100 meters underground on 
a 27 kilometer long circular particle accelerator (the Large Hadron Collider, LHC) 
running under the Switzerland-France border. The CMS and ATLAS experiments at CERN 
announced the discovery of Higgs boson in 2012, leading to the awarding of the 2013 
Nobel Prize in Physics.
\end{abstract}

\section{Introduction and Motivation} 

The data produced by the four experiments at the Large Hadron Collider
(LHC)~\cite{LHCPAPER} or similar High Energy Physics (HEP) experiments
requires a significant amount of human and computing resources which
cannot be provided by research institutes or even countries. For these
reasons the various parties involved created the Worldwide LHC Computing
Grid (WLCG) in order to tackle the data processing challenges posed by
such a large amount of data. The WLCG consists of a highly federated
union of 170 computing centers sparse in 42 countries and it represents
an admirable example of international organization. The Compact Muon
Solenoid (CMS) experiment~\cite{CMSDET} at the LHC alone uses the order
of 100,000 x86\_64 cores for its data analysis and similar happens for
the other general purpose LHC experiment, ATLAS\@. Moreover, as the
LHC and the experiments will undergo planned luminosity upgrades over
the next 15 years~\cite{HLLHC}, its dataset size will increase of 2--3
orders of magnitude, which will require additional efforts to increase
its processing capacity.

High Throughput Computing (HTC) converged on x86 and Linux at around 2000 and 
was driven by commodity market. The convergence was a significant simplification 
which subsequently enabled the current model of the WLCG -- build once, run 
everywhere. On the other hand the commodity marked was driven by price/performance, 
but power use and density were scaling with performance thus creating a power wall 
for further substantial improvements. Even multi-core is only a 
stop-gap solution. Moreover, the focus is also shifting from price/performance to 
watt/performance. Given overall power wall limitation lightweight, general 
purpose, high density and low power systems with specialised accelerators become
more interesting for HTC to increase overall system throughput and power 
efficiency.

% Above is not fully true about ARMv8 64-bit, I wouldn't call ThunderX as 
% lightweight system, but it's used in a  high density system (cores per U). 

ARM Ltd. has dominated mobile and embedded marked due to focus on low power 
solutions. In addition they have a business model where intellectual 
property (IP)  is licensed rather than produced. ARM Ltd. provides licenses for ISA 
and various in-house designed IP blocks (e.g. CPU, GPU, cache coherent 
interconnect). This allows the specialisations described earlier and creates 
healthy market competition.

In the previous paper~\cite{ACAT2014_HTC_PHI_XGENE1} we provided information about CMS 
Software (CMSSW) porting efforts to ARMv8 64-bit using various software models 
and hardware development platform. We also provided a first 
overview~\cite{CHEP2015ARMV8TIER3} of ARMv8 64-bit. In this paper we provide a 
technical look into the first operational prototype of ARMv8 64-bit demonstrator 
cluster at Princeton University.

% In above we add references to our previous papers about CMSSW and Tier-3.

% This could be slitted. In 'Motivation' we could talk about why ARM is a 
% possible solution like it was done in ACAT paper. 

% http://ark.intel.com/products/27219/Intel-Xeon-Processor-5160-4M-Cache-3_00-GHz-1333-MHz-FSB

\section{Hardware and Setup}
The initial demonstrator consisted of APM Mustang/XC-1 development platform and 
x86\_64 machine. APM Mustang is based on APM X-Gene 1 (8 
physical cores at 2.4GHz) with 16GB DDR3 memory. The platform is a worker node (WN) 
machine and provides 8 static slots, each with 2GB of memory. Fedora 19 Linux 
distribution was used for WN due to similarities to Scientific Linux, which is
widely used in WLCG. x86\_64 machine is a computing element (CE) and is based on two 
socket Intel Xeon 5160 (2 physical cores at 3.0GHz, no HT, Q1 2009) with 16GB of 
DDR3 memory. It is running with Springdale Linux 6.7 (Pisa).

The local batch system, i.e. between CE and WN on site, is based on HTCondor 
(8.2.8). CE is also an entry point for computing resources at site from pilot 
factory point of view.
We prepared a stripped down version of HTCondor for ARMv8 64-bit and 
packaged in a tarball for the pilot factory. The pilot factory talks directly 
to CE on site and sends pilot jobs (see figure~\ref{fig:how_pilots_work}). 
The pilot job downloads and unpacks our earlier prepared HTCondor based on 
distribution and architecture, then executes HTCondor \texttt{condor\_startd} 
process (see figure~\ref{fig:pilots_internal}) which connects back to 
\texttt{condor\_collector} at CMS computing infrastructure. This allows creating a 
common computing pool based on a single HTCondor version used by CMS. Then the 
pool is used for submitting jobs and receiving results. The computing 
infrastructure hides the complexity of all computing sites and their batch 
systems for users.

% These are just copy & paste from a poster -- A pilot­ based Grid Workload 
% Management System.

% We talk about [local] SE (Storage Element), but we should also add SE box to 
% these pictures 

\begin{figure}[ht]
  \centering
  \begin{minipage}{7.0cm}
    \includegraphics[width=7.0cm]{images/wlcg_pilot_overview_new.png}
    \caption{\label{fig:how_pilots_work}Pilots and virtual private pools}
  \end{minipage}
  \hspace{0.5cm}
  \begin{minipage}{7.0cm}
    \includegraphics[width=7.0cm]{images/wlcg_schema_new.png}
    \caption{\label{fig:pilots_internal}Schematic overview}
  \end{minipage}
\end{figure}

CMS provides two separate tools for injecting workloads: \textit{ProdAgent} --  
production workflows and \textit{CRAB3} -- user analysis workflows. Decision 
was made to use CRAB3 for demonstration as the team was familiar with code base and 
infrastructure behind it.

Local storage element (SE) is required in order to achieve high job efficiency 
levels, but it is not essential for demonstration purposes. The team decided to 
allow low job efficiency levels caused by remote I/O needed to fetch input data 
from various computing sites. This also allowed stressing \textit{xrootd} client 
testing in CMSSW.

% The point is that we need a tarball in pilot factory specifically for aarch64
% and 'common' operating system (Centos/RHELSA/Fedora). It's always OS + arch.
% HTCondor version is tarball _must_ (based on policy) match a version used by 
% CMS. From CMS perspective 'GRID' (that common computing pool) is running exact
% same version across all slots.

% We cheated and tricked the system to think that our condor is at the same 
% version in ITB pool/factory/whathever.

% Basically this is a short talk about HW (boring) and a very long talk (boring) 
% about how pilot-based Grid Workload Management System (glideinWMS) works. Then
% explaining that we need a tarball'd HTCondor and why.    

% NOTE. This explain where CRAB3 fits in and how stageout works. It's important 
% to mention that GFAL2-copy is the only thing that worked for file transfers
% between sites (i.e. SE). 

% Our setup does not have local SE, we only do remote reads and stageouts to 
% remove SE.

\section{Issues and Lessons Learned}
Deploying a new architecture to CMS and WLCG infrastructure revealed several 
issues:
\begin{itemize}
\item A full port and rebuild of OSG repositories are required. In addition it 
must be synchronised with x86\_64 repositories to comply with CMS policies, i.e.
exact version of HTCondor is used for all pilot jobs.

\item Originally only Fedora 19 build was available for APM Mustang and it 
was the most similar to Scientific Linux, RHEL or CentOS used in WLCG. It 
took us a great length of time porting various packages and repositories 
(incl. CMSSW) to ARMv8 64-bit, and Fedora 19 repositories were deleted before we 
managed to build all packages required (e.g., \textit{GFAL2}) for demonstrator 
cluster to operate as needed. In addition to that Fedora 19 repository contained 
packages with broken dependencies, which required us to rebuild a number of 
system packages (e.g. Perl and various dependent modules).

\item An issue~\cite{BINUTILS_BUGZILLA_CVMFS} in \texttt{ld.bfd} linker was 
discovered and blocked CVMFS package from building. The issue was resolved 
upstream by ARM Ltd. compiler team and will be in \textit{binutils} 2.26 release.
Also it was communicated back to Red Hat~\cite{BINUTILS_BUGZILLA_CVMFS_RH} and the 
bug fix will be part of RHELSA DP\footnote{Red Hat Enterprise Linux Server for 
ARM Developer Preview}/CentOS 7.2 (distributions for the next demonstrator).

\item CRAB3 depends on COMP repository which is managed separately from CMSSW 
repository. COMP repository was not ported to ARMv8 64-bit and was deemed by 
team not necessary for operations. CRAB3 was modified to work avoid explicit 
references to Python 2.6 package installation location from COMP repository and 
instead to use Python 2.7 package from CMSSW repository or/and standard system 
location.

\item Additional code or/and code modifications on top of CMSSW release can be 
provided by user and is known as \textit{user code}. The user code is compiled 
on submission machine and then packaged into \textit{sandbox} by CRAB3 client. 
Currently our demonstrator cluster only supports user code only if submission 
machine and WN have a matching architecture. There is no infrastructure to 
compile the user code to various different architectures before job submission.

\item CRAB3 sets \texttt{GLIDEIN\_REQUIRED\_OS}, \texttt{OpSysMajorVer}
and \texttt{Target.Arch} in \texttt{Requirements} attribute of the job 
\textit{ClassAd}. We had to manually modify job requirements on \textit{shedd} 
machine (see figure~\ref{fig:pilots_internal}) after submitting a task using CRAB3. 
\texttt{GLIDEIN\_REQUIRED\_OS} was set to \texttt{any}, \texttt{OpSysMajorVer} 
to \texttt{19} and finally \texttt{Target.Arch} to \texttt{aarch64} to make sure 
our jobs are matched to slots available on our AArch64 WN.

\item CRAB3 and CMS in general only support a single \texttt{SCRAM\_ARCH} per 
release cycle. Our submission and execution \texttt{SCRAM\_ARCH} differs,
\texttt{slc6\_amd64\_gcc481} was used on submission node and 
\texttt{fc19\_aarch64\_490} on WN. We forced CRAB3 to think that we have
\texttt{fc19\_aarch64\_490} on the submission node.

% poor? Well, statistics are available in my data of investigation on GRID
% IIRC, FMA is only available on ~50% of cores.

% Also this is the only point which doesn't directly relate to computing 
% infrastructure.

%\item We have learned by debugging \textit{HepMC} package in Fedora 23 that part 
%of numerical differences could be attributed to FMA instructions. These 
%instructions are not used on x86\_64 as we use \texttt{generic} target and FMA 
%availability on WNs is poor. However AArch64 has FMA enabled by default and it
%provides higher precision than combination of individual instructions due to 
%rounding errors.

% ^^^ Is this example worth of in-depth overview? Maybe it should be part of 
% results.

\end{itemize}
% We need a full port / rebuild of OSG in sync with x86\_64. E.g., we need to 
% keep HTCondor at the same version as what is used by CMS. (Is our HTCondor source
% OSG repo?)

% Fedora 19 is dead. Deleted repositories were creating issues getting software
% compiled (e.g. gfal2). It was supposed to be done for ACAT, never be dragged
% for that long. Other issues that it never had massive rebuilt, thus a lot of 
% dependencies were broken.

% ld.bfd linker issue failing CVMFS compilation (resolved and in RHELSA DP/CentOS
% 7.2)

% CRAB3 and dependency on COMP environment. We did not use COMP Python. Justas 
% cleaned CRAB3 to use system or/and CMSSW python and that works flawlessly.
% There is no COMP repositories on AArch64 and we don't want to have one.

% Via CRAB3 you can only submit jobs without user code. There are no 
% infrastructure to prepare sandbox for different architectures (should not be hard)
% to implement MQ based build farm.

% Once you submit a job via CRAB3, you have to manually modify matching rule in 
% shedd server, because CRAB3 assumes only X86\_64, single OS and single 
% SCRAM\_ARCH. Justas had to force CRAB3 to think SCRAM\_ARCH=fc19\_aarch64\_gcc490

% CRAB3 part is a hacky one, because pushing changes here one could get a 
% high resistance.

\section{Results}
We have managed to create a task on x86\_64 machine with \texttt{SCRAM\_ARCH} set to 
\texttt{slc6\_amd64\_gcc481} at \textit{T2\_CH\_CERN} site and submit it using CRAB3 to
\textit{T3\_US\_Pricneton\_ARM} site with target \texttt{SCRAM\_ARCH} set to 
\texttt{fc19\_aarch64\_gcc490}. The jobs within the task were successfully 
reading input files remotely from T2\_CH\_CERN using xrootd protocol. 
Then output files were staged out to \textit{T2\_BR\_SPRACE} using \texttt{gfal2-copy} 
before transferring them back to T2\_CH\_CERN for our inspections. Figures 
\ref{fig:cms_dashboard} and \ref{fig:cms_dashboard2} from CMS Dashboard 
monitoring website shows job succeeding at T3\_US\_Pricneton\_ARM site.

\begin{figure}[ht]
  \centering
  \includegraphics[width=16cm]{images/chart_jobs.png}
  \caption{\label{fig:cms_dashboard}Successful task execution on ARMv8 64-bit WN}
\end{figure}

\begin{figure}[ht]
  \centering
  \includegraphics[width=16cm]{images/arm_site_works_3.png}
  \caption{\label{fig:cms_dashboard2}Successful job execution on ARMv8 64-bit WN}
\end{figure}

We used the available infrastructure to run TTBar 13TeV no pile-up RelVal 
workflow of a standard size -- 9000 events. The goal was to inspect numerical 
stability in \textit{RECO} step thus \textit{DIGI} step was done on x86\_64 
machine and re-used for both architectures. \texttt{CMSSW\_7\_2\_0\_pre1} release 
available on CVMFS~\cite{ACAT2014_HTC_PHI_XGENE1} was used for all tests. 950 differences 
were detected with majority being non significant, i.e. the difference being close 
to zero. Figure \ref{fig:examples_reco_diff} provides examples of numerical 
differences between x86\_64 and AArch64 architectures in RECO step.

\begin{figure}[ht]
  \centering
  \caption{\label{fig:examples_reco_diff}Examples of observed numerical differences in RECO step}
  \begin{minipage}{7.0cm}
    \includegraphics[width=7.0cm]{images/allc_EcalRecHitsSorted_reducedEcalRecHitsEE__RECO_obj_obj_chi2.png}
  \end{minipage}
  \hspace{0.5cm}
  \begin{minipage}{7.0cm}
    \includegraphics[width=7.0cm]{images/allc_recoPFCandidates_particleFlow__RECO_obj_eta17.png}
  \end{minipage}
\end{figure}

We have learned by debugging \textit{HepMC} package in Fedora 23 Beta (also 
included in CMSSW) that part of numerical differences could be attributed to FMA 
instructions. These instructions are not used on x86\_64 as we use a \texttt{generic} 
target and FMA availability on WNs is only 23\% on 188,422 sampled cores. 
On the other hand AArch64 has FMA enabled by default and it provides a higher 
precision than combination of individual instructions due to rounding errors at 
intermediate states.

%\item We have learned by debugging \textit{HepMC} package in Fedora 23 that part 
%of numerical differences could be attributed to FMA instructions. These 
%instructions are not used on x86\_64 as we use \texttt{generic} target and FMA 
%availability on WNs is poor. However AArch64 has FMA enabled by default and it
%provides higher precision than combination of individual instructions due to 
%rounding errors.

% We submitted TTBar 13TeV noPU 9000 events (full RelVal) from x86 machine at 
% T2_CH_CERN, got inputs from T2_CH_CERN (later also from Brazil and US), 
% successfully executed and stageout results to T2 Brazil site (everyone has write 
% access on that site) before downloading them to T2_CH_CERN.

% We used CMSSW_7_2_0_preX, which is available on CVMFS.

% Looked at RECO results, similar results as in ACAT paper.

% boring section.

% We did discover recently (not via these activities, but in Fedora upstream) 
% that HepMC was failing due to use of FMA instructions (floating point const 
% folding and contraction). We don't use FMA in x86. This is something that has 
% affect on numerical comparison.

\section{Future}
A number of important steps are required for a stable and up date with respect to 
x86\_64 demonstrator cluster:
\begin{itemize}
\item The second generation prototype will re-use the existing CE machine and APM 
Mustang for WN. Additional WNs will be added based on ARMv8 64-bit production 
level platform -- HP Moonshot chassis with six m400 cartridges. m400 cartridge 
contains APM X-Gene 1 silicon as in APM Mustang. In additional to that 128GB M2 
SSD drive and 64GB of memory is used. This allows us to have 8GB/core static slot 
configuration for majority ARMv8 64-bit slots available in the demonstrator cluster. 
Furthermore m400 firmware is now fully public.

% There could be a table comparing m400 vs APM Mustang hardware


%\begin{table}[h]
%\caption{\label{specs}ARMv8 64-bit WN Hardware}
%\begin{center}
%    \begin{tabular}{lrrl}
%        \br\
%                          & APM Mustang    & m400\\
%      \mr\
%      \mr\
%Processor               & APM X-Gene 1 (APM883408)   & APM X-Gene 1 (APM883408) \\
%Frequency               & 2.4GHz         & 2.4GHz \\
%        Physical Cores          & 8              & 8 \\
%        Memory                  & 16GB           & 64GB \\
%        Storage & 500GB HDD & 120GB M2 SSD \\
%        
%        \br\
%    \end{tabular}
%\end{center}
%\end{table}


\item We have selected RHELSA DP and CentOS 7.2 as a common Linux distribution to be 
used for AArch64 activities in the future. The same operating system should be 
used on x86\_64 and AArch64 for WLCG activities to keep the experience for 
administrators and users similar. CentOS for AArch64 is built from the same 
source packages as RHELSA DP thus making them interchangeable. CentOS is also 
NDA-free and does not require additional agreements to be signed. CERN and 
Princeton University signed partnership agreements with Red Hat to be part of 
Partners Early Access Program (PEAP) in order to get
early access to REHLSA DP. In addition to that RHEL and RHELSA 7.2 contains required 
thread local storage (TLS) fixes in \textit{glibc}~\cite{GLIBC_BUGZILLA_RH} needed 
for CMSSW operations.

% TODO: add RH BZ items for glibc and binutils (in previous section)

\item The default CMSSW development branch (\texttt{CMSSW\_7\_6\_X}) cannot compile 
on AArch64. CMSSW needs to finish migration from ROOT 6.02 to 6.04 in order for 
the same code base to be compilable on multiple architectures. ROOT 6 is based 
on LLVM/Clang and in 6.04 it moved from old JIT to OrcJIT interface.

\item We need to discuss with OSG regarding adding additional architecture to their 
repositories and release cycles. This also includes providing AArch64 capable 
Koji build machines or/and hardware in their build infrastructure.

\item Hardware needs to be provided to CVMFS developers community in order to receive 
the initial RPMs from the official source.

\item The current demonstrator was broken due to various development 
activities being tested in ITB (Integrated Test Bed) computing resource pool. We 
should move to production (PROD) global pool to avoid having unexpected downtimes.

\item Little has been done to investigate numerical differences between x86\_64 
and AArch64 architectures. In addition to that further cleanups of C++ undefined 
behaviour, dependencies on random memory and similar issues need to be cleaned up 
as they could affect numerical comparison.

\item Local SE is needed to minimise remote I/O impact on jobs efficiency.
\end{itemize}

% Moving to new hardware HP Moonshot + m400 (hardware specs here?)

% Common operating system we love, RHELSA DP 7.2 (not yet released) and CentOS 
% 7.2 (will be cooked once RHELSA is released)

% ROOT 6.04 and OrcJIT means that CMSSW will be able to generate, compile and 
% use dictionaries on aarch64. Unknown timescale for migration to 6.04, most 
% likely out of question for CMSSW_7_6_X.

\section{Conclusions}
We continued our efforts in evaluating ARMv8 64-bit (AArch64) as alternative 
architecture for CMS computing needs. While not final, our conclusions shows 
that heterogeneous computing environment using CRAB3 is a plausible option 
if further improvement to CRAB3 would be made. From numerical stability point 
of view we see a number of differences, however majority of them being 
non-significant, i.e. close to zero.

We look forward to continue our efforts in improving our ARMv8 64-bit 
demonstrator cluster at Princeton University and improving ARMv8 64-bit status 
for scientific computing in general via various collaborations.

\section*{Acknowledgements}
This work was partially supported by the National Science Foundation, under
Cooperative Agreement PHY-1120138, and by the U.S. Department of Energy. We 
would like to express our gratitude to Red Hat, ARM Ltd. and Cavium for helping 
to solve some of the issues along the way.

\section*{References}
\bibliographystyle{unsrt}
\bibliography{linaro_15_aarch64_wlcg}

\end{document}
